class Wall {
	constructor(id) {
		this.id = id;
		this.texture = ImageHandler.wall_textures[id];
	}

	get ignore_raycast() {
		return this.texture.transparent;
	}
}

class Room {
	constructor() {
		this.walls = [];
	}

	setWall(id, x, y) {
		if ( this.walls[x] == undefined ) {
			this.walls[x] = [];
		}

		this.walls[x][y] = new Wall(id);
	}

	getWall(x, y) {
		if ( this.walls[x] != undefined ) {
			return this.walls[x][y];
		}

		return undefined;
	}

	setRow(y, line, conversion_data={}) {
		let x = 0
		for ( let character of line ) {
			if ( conversion_data[character] ) {
				character = conversion_data[character];
			}

			if ( character != " " ) {
				this.setWall(character, x, y);
			}
			x += 1;
		}
	}
}