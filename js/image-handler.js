class TextureColumn {
	constructor(texture, i) {
		let column_canvas = document.createElement("canvas");
		column_canvas.width = 1;
		column_canvas.height = texture.height;
		let column_context = column_canvas.getContext("2d");
		column_context.drawImage(texture, i, 0, 1, texture.height, 0, 0, 1, texture.height);

		this.height = column_canvas.height;
		this.column_canvas = column_canvas;
		this.transparent = false;

		let data = column_context.getImageData(0, 0, 1, column_canvas.height).data;
		for (let i = 3, n = data.length; i < n; i+=4) {
			if (data[i] < 255) {
				this.transparent = true;
				break;
			}
		}
	}
}

class WallTexture {
	constructor(id) {
		this.id = id;
		this.width = 0;
		this.height = 0;
		this.columns = undefined;

		this.transparent = false;
	}

	onload(texture) {
		this.width = texture.width;
		this.height = texture.height;

		this.columns = [];

		for (let i = 0; i < texture.width; i++) {
			let column = new TextureColumn(texture, i);
			this.columns.push(column);

			if ( column.transparent ) {
				this.transparent = true;
			}
		}
	}
}

ImageHandler = {};
ImageHandler.wall_textures = {};
ImageHandler.loadWallTexture = function(id, url) {
	ImageHandler.wall_textures[id] = new WallTexture(id);
	let img = new Image();
	img.onload = function() {
		ImageHandler.wall_textures[id].onload(img);
	}
	img.src=url;
}

ImageHandler.loadWallTexture("metal_wall", "img/walls/metal_wall.png");
ImageHandler.loadWallTexture("fence", "img/walls/fence.png");