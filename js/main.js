let canvas = document.getElementById("main-canvas");
let res_slider = document.getElementById("res-slider");
let frame_counter = document.getElementById("framerate");

function resSliderThing() {
	canvas.height = Math.floor(res_slider.value);
	canvas.width = Math.floor(res_slider.value * 4/3);
}

c_data = {
	"#": "metal_wall",
	"F": "fence"
}

let default_room = new Room();
default_room.setRow(7, " # ", c_data)
default_room.setRow(6, "# ####", c_data)
default_room.setRow(5, "#     #", c_data)
default_room.setRow(4, " #   #", c_data)
default_room.setRow(3, "#     #", c_data)
default_room.setRow(2, "# #F##", c_data)
default_room.setRow(1, "#     #", c_data)
default_room.setRow(0, " #####", c_data)

let game = new GameCanvas(canvas, default_room);
game.inputs.addAxis( "forwards", new Axis(87, 83) );
game.inputs.addAxis( "sideways", new Axis(68, 65) );
game.inputs.addAxis( "spinning", new Axis(39, 37) );

let character = {
	rotation: -90,
	x: 5.5,
	y: 1.5
};

let average_span = 1;
let end_average = Date.now() + (average_span * 1000)
let frame_count = 0;
function render(delta_time) {
	let current_time = Date.now();
	if ( current_time >= end_average ) {
		frame_counter.innerHTML = Math.round(frame_count / average_span);
		frame_count = 0;
		end_average = current_time + (average_span * 1000);
	}
	frame_count += 1;

	game.renderer.updateCameraPosition(character.x, character.y, character.rotation);
}

//TODO: Calculate in reference to delta time.
function game_loop(delta_time) {
	let direction_x = Math.sin(character.rotation*deg2rad);
	let direction_y = Math.cos(character.rotation*deg2rad);

	let forwards = game.inputs.getAxisRaw("forwards");
	let sideways = game.inputs.getAxisRaw("sideways");
	let spinning = game.inputs.getAxisRaw("spinning");

	let speed_multiplier = 0.002;
	let spin_multiplier = 0.08;


	let forwards_x = ( forwards * direction_x * speed_multiplier ) * delta_time;
	let forwards_y = ( forwards * direction_y * speed_multiplier ) * delta_time;

	let sideways_x = ( -sideways * direction_x * speed_multiplier ) * delta_time;
	let sideways_y = ( sideways * direction_y * speed_multiplier ) * delta_time;

	character.x += forwards_x + sideways_y;
	character.y += sideways_x + forwards_y;
	character.rotation += spinning * spin_multiplier * delta_time;
}

let max_fps = 60;
let start_time = Date.now();
let frame_life = 1000 / max_fps;

function setMaxFPS(fps) { max_fps = fps; frame_life = 1000 / max_fps; }

function animation_frame() {
	requestAnimationFrame(animation_frame);

	let current_time = Date.now();
	let delta_time = current_time - start_time;

	if (delta_time > frame_life) {
		render(delta_time);
		game_loop(delta_time);

		start_time = current_time - (delta_time % frame_life);
	}
}

animation_frame();