class Renderer {
	constructor(canvas, room) {
		this.canvas = canvas;
		this.context = canvas.getContext("2d");
		this.context.imageSmoothingEnabled = false;
		this.fov = 70;
		this.room = room;
	}

	renderBackground(colour1, colour2) {
		let width = this.canvas.width;
		let height_split = this.canvas.height/2;

		this.context.fillStyle = colour1;
		this.context.fillRect(0, 0, width, height_split);

		this.context.fillStyle = colour2;
		this.context.fillRect(0, height_split, width, height_split);
	}

	renderColumn(image, x, height, dataWrap=0) {
		if ( image == undefined || image.columns == undefined ) {
			this.context.fillStyle = "black";
			this.context.fillRect(x, (this.canvas.height-height)/2, 1, height);
		} else {
			let texture_column = image.columns[dataWrap % image.width];
			this.context.drawImage(texture_column.column_canvas, x, (this.canvas.height-height)/2, 1, height);
		}
	}

	getWall(x, y) {
		return this.room.getWall(x, y);
	}

	raycast(x, y, ray_direction_x, ray_direction_y, iteration_limit=10) {
		let map_x = Math.floor(x);
		let map_y = Math.floor(y);

		let side_distance_x;
		let side_distance_y;

		let delta_distance_x = Math.abs(1 / ray_direction_x);
		let delta_distance_y = Math.abs(1 / ray_direction_y);

		let step_x;
		let step_y;

		let hit = false;
		let side = false; // True = X-Axis Wall, False = Y-Axis Wall

		if (ray_direction_x < 0) {
			step_x = -1;
			side_distance_x = (x - map_x) * delta_distance_x;
		} else {
			step_x = 1;
			side_distance_x = (map_x + 1.0 - x) * delta_distance_x;
		} if (ray_direction_y < 0) {
			step_y = -1;
			side_distance_y = (y - map_y) * delta_distance_y;
		} else {
			step_y = 1;
			side_distance_y = (map_y + 1.0 - y) * delta_distance_y;
		}

		let hits = [];

		let wall_id;
		let iterations = 0;
		while (!hit && iterations < iteration_limit) {
			if (side_distance_x < side_distance_y) {
				side_distance_x += delta_distance_x;
				map_x += step_x;
				side = false;
			} else {
				side_distance_y += delta_distance_y;
				map_y += step_y;
				side = true;
			}
			let wall = this.getWall(map_x,map_y);
			if ( wall != undefined ) {
				wall_id = wall.id;

				let wall_distance;
				if (side) {
					wall_distance = (map_y - y + (1 - step_y) / 2) / ray_direction_y;
				} else {
					wall_distance = (map_x - x + (1 - step_x) / 2) / ray_direction_x;
				}

				let hit_data = {
					side: side,
					distance: wall_distance,
					wall_id: wall_id
				}
				hits.push(hit_data)

				if ( !wall.ignore_raycast ) {
					hit = true;
				}
			}

			iterations += 1;
		}

		return hits;
	}

	updateCameraPosition(x, y, angle) {
		let width = this.canvas.width;
		let height = this.canvas.height;
		let angle_change = this.fov/width;
		let angle_start = angle - this.fov/2;
		angle_start = angle_start%360;

		this.renderBackground("black", "gray");

		for(let i = 0; i < width; i++) {
			let current_angle = ( angle_start + (angle_change*i) );
			let current_angle_rad = current_angle*deg2rad;
			let ray_direction_x = TrigLookup.sin(current_angle_rad);
			let ray_direction_y = TrigLookup.cos(current_angle_rad);

			let raycast_hits = this.raycast(x, y, ray_direction_x, ray_direction_y);
			let angle_from_camera_plane = current_angle_rad - (angle-90)*deg2rad;

			for(let raycast_index = raycast_hits.length - 1; raycast_index >= 0; raycast_index--) {
				let raycast = raycast_hits[raycast_index];

				let perp_distance = TrigLookup.sin(angle_from_camera_plane) * raycast.distance;
				let line_height = Math.ceil(height / perp_distance);
				let texture = ImageHandler.wall_textures[raycast.wall_id];
				let texture_width = 512;
				if ( texture != undefined ) {
					texture_width = texture.width;
				}

				let wall_x;
				if ( raycast.side ) {
					wall_x = x + raycast.distance * ray_direction_x;
				} else {
					wall_x = y + raycast.distance * ray_direction_y;
				}
				wall_x -= Math.floor(wall_x);
				let texX = wall_x * (texture_width-1);
				if(raycast.side == 0 && ray_direction_x > 0) texX = texture_width - texX - 1;
				if(raycast.side == 1 && ray_direction_y < 0) texX = texture_width - texX - 1;

				texX = Math.floor(texX);

				this.renderColumn(texture, i, line_height, texX);
			}
		}
	}
}