TrigLookup = {};
TrigLookup.max_detail = 3;

TrigLookup.sin_data = {};
TrigLookup.cos_data = {};

TrigLookup.sin = function(angle) {
	if (angle < 0) { angle += doublepi; }	
	angle %= doublepi;
	let lookup_value = Math.trunc(angle * 10**TrigLookup.max_detail);

	let output = TrigLookup.sin_data[lookup_value];
	if ( output != undefined ) {
		return output;
	}

	return 0;
}

TrigLookup.cos = function(angle) {
	if (angle < 0) { angle += doublepi; }
	angle %= doublepi;	
	let lookup_value = Math.trunc(angle * 10**TrigLookup.max_detail);

	let output = TrigLookup.cos_data[lookup_value];
	if ( output != undefined ) {
		return output;
	}

	return 0;
}

TrigLookup.generateLookupData = function() {
	let detail_power = 10**TrigLookup.max_detail;
	let max_lookup = Math.trunc(doublepi * detail_power);

	for (let i = 0; i <= max_lookup; i++) {
		let actual_angle = i / detail_power;

		TrigLookup.sin_data[i] = Math.sin(actual_angle);
		TrigLookup.cos_data[i] = Math.cos(actual_angle);
	}
}

TrigLookup.generateLookupData();