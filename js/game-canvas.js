class GameCanvas {
	constructor(canvas, room) {
		this.renderer = new Renderer(canvas, room);
		this.inputs = new InputHandler();
	}
}