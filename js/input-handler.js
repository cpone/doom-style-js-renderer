class Axis {
	constructor(positive, negative) {
		this.positive = positive;
		this.positive_on = false;

		this.negative = negative;
		this.negative_on = false;

		this.maximum = 1;
		this.default = 0;
		this.minimum = -1;

		this.current_value = this.default;
		this.target_value = this.default;
	}

	triggerPositive() { this.positive_on = true; }
	untriggerPositive() { this.positive_on = false; }

	triggerNegative() { this.negative_on = true; }
	untriggerNegative() { this.negative_on = false; }

	updateAxis() {
		this.target_value = ( this.maximum * this.positive_on ) + ( this.minimum * this.negative_on );
		if ( this.target_value == 0 ) {
			this.target_value = this.default;
		}

		let interp_val = 0.05;
		if ( this.current_value > this.target_value ) {
			this.current_value -= interp_val;
		} else if ( this.current_value < this.target_value ) {
			this.current_value += interp_val;
		}
	}
}

class InputHandler {
	constructor() {
		this.axes = {};
		document.addEventListener( "keydown",	this.keyDown.bind(this),	true);
		document.addEventListener( "keyup",		this.keyUp.bind(this),		true);

		setInterval( this.updateAxes.bind(this), 1000/60 );
	}

	addAxis(id, axis) {
		this.axes[id] = axis;
	}

	keyDown(event) {
		let keycode = event.keyCode;
		for ( let id in this.axes ) {
			let axis = this.axes[id];

			if ( keycode == axis.positive ) {
				axis.triggerPositive();
			}
			if ( keycode == axis.negative ) {
				axis.triggerNegative();
			}
		}
	}

	keyUp(event) {
		let keycode = event.keyCode;
		for ( let id in this.axes ) {
			let axis = this.axes[id];

			if ( keycode == axis.positive ) {
				axis.untriggerPositive();
			}
			if ( keycode == axis.negative ) {
				axis.untriggerNegative();
			}
		}
	}

	updateAxes() {
		for ( let id in this.axes ) {
			let axis = this.axes[id];
			axis.updateAxis();
		}
	}

	getAxis(id) {
		let axis = this.axes[id];
		return axis.current_value
	}

	getAxisRaw(id) {
		let axis = this.axes[id];
		return axis.target_value
	}
}