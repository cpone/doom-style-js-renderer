# Doom-Style-JS-Renderer

A doom style canvas renderer written solely in javascript.

Check it out here: https://projects.connieprice.co.uk/renderer/

Controls: WASD + Arrow Keys
